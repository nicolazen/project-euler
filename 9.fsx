(*
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a^2 + b^2 = c^2

For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
*)

let solution =
    let triplets =
        [ for x in 1 .. 999 do
            for y in x .. (999 - x) do
                (x, y, (1000 - x - y)) ]

    triplets
    |> List.filter (fun (a, b, c) -> ((a * a) + (b * b)) = (c * c))
    |> List.item 0
    |> fun (a, b, c) -> [ a; b; c ]
    |> List.reduce (*)

printfn "%i" solution

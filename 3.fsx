(*
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
*)

let isPrime (nr: float) =
    let rec factor smallerNr =
        if nr % smallerNr = 0.0 then
            match smallerNr with
            | 1.0 -> true
            | _ -> false
        else
            factor (smallerNr - 1.0)
    if nr = 1.0 then true
    elif (nr % 2.0 = 0.0 || nr % 3.0 = 0.0 || nr % 5.0 = 0.0) && nr > 5.0 then false
    else factor (nr - 1.0)


let largestPrimeFactor nr =
    let nextPrime current =
        let rec findNextPrime nr =
            match (isPrime nr) with
            | true -> nr
            | false -> findNextPrime (nr + 1.0)
        findNextPrime (current + 1.0)

    let rec nextDivider nr divider =
        match nr % divider = 0.0 with
        | true -> divider
        | false -> nextDivider nr (nextPrime divider)

    let rec getDividers nr dividers =
        let divider = nextDivider nr 2.0
        let dividers' = divider :: dividers
        let newNr = nr / divider
        match newNr with
        | 1.0 -> dividers'
        | _ -> getDividers newNr dividers'


    let factors = getDividers nr []
    factors |> Seq.max


let nr = 600851475143.0
printfn "The largest prime factor of %f is %f" nr (largestPrimeFactor nr)

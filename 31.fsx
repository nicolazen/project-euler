﻿// The approach used is to, starting from the bigger coin towards the smaller coin, add
// iteratively the bigger missing coin up to the desired amount. In case the added coin
// overflows the target amount of 2 pounds, we will remove it and add smaller coins up
// to the desired amount
let nrCombinations =
    // helper arrays to hold the amount of coins and the value of each coin type
    let v = [ 1; 2; 5; 10; 20; 50; 100; 200 ]
    let a = [ 0; 0; 0; 0; 0; 0; 0; 0 ]

    // helper function to calculate the runnig total based on the combination of number
    // of coins and values of them
    let currentTotal a =
        v
        |> List.zip a
        |> List.map (fun (a, b) -> a * b)
        |> List.reduce (+)

    // Returns the index of the bigger coin that is smaller than the missing amount to
    // reach 2 pounds
    let findBiggerCoinMissing remainder currentIndex =
        // If no remainder, than return the same coin, next iteration will acknowledge the
        // fact that we reached the target
        if remainder = 0 then
            currentIndex
        else
            let newIndex = v |> List.findIndexBack (fun c -> c <= remainder)
            // To make sure we find only unique solutions, do not go back more than the
            // starting index. The other solutions will be found later.
            if newIndex < currentIndex then currentIndex else newIndex

    // Most important function, it will create the combinations of the a array in an interative
    // way
    let rec finder i found (amounts: int list) =
        let total = currentTotal amounts
        // Target reached. Increase the amount of solutions found, and set the amount of current
        // index coins to zero, as we are moving now to the smaller coin size
        if total = 200 then
            if (i = 0)
            then (found + 1)
            else finder (i - 1) (found + 1) (amounts.[0..(i - 1)] @ [ 0 ] @ amounts.[(i + 1)..7])
        // Target overflown. Move to the smaller coins and discard all bigger coins. The next
        // iterations will select more appropriate coin combinations
        elif total > 200 then
            finder (i - 1) found (amounts.[0..(i - 1)] @ [ for i in 0 .. (7 - i) -> 0 ])
        // Target still to be reached. Increase the amount of the current coin by 1 and test again
        else
            let i' = findBiggerCoinMissing (200 - total) i
            finder i' found (amounts.[0..(i - 1)] @ [ amounts.[i] + 1 ] @ amounts.[(i + 1)..7])

    // kick off iterations
    finder 7 0 a

printfn "Result: %i" nrCombinations

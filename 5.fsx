let dividers = [| 2 .. 20 |]

let checkNoRemainder nr =
    let rem =
        dividers |> Array.sumBy (fun d -> nr % d)
    rem = 0

let result =
    Seq.initInfinite (fun n -> n * 20)
    |> Seq.filter (fun n -> n > 0)
    |> Seq.filter checkNoRemainder
    |> Seq.item 0

printfn "%i" result

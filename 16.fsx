(*
2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 2"1000?
*)

let result =
    let power = (2.0 ** 1000.0).ToString("F")
    power.ToCharArray()
    |> Array.filter (fun x -> x <> '.')
    |> Array.map string
    |> Array.map int
    |> Array.sum

printfn "%i" result

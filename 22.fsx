(*
Using names.txt (right click and 'Save Link/Target As...'), a 46K text file
containing over five-thousand first names, begin by sorting it into alphabetical
order. Then working out the alphabetical value for each name, multiply this
value by its alphabetical position in the list to obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN, which is
worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would
obtain a score of 938 × 53 = 49714.

What is the total of all the name scores in the file?
*)

open System.IO

let filePath = "22_input.txt"

let lettersNumberMapping = List.zip [ 'A' .. 'Z' ] [ 1 .. ((int 'Z') - (int 'A') + 1) ] |> Map

let names =
    let fileContent = File.ReadLines(filePath) |> Seq.head

    let names =
        fileContent.Split [| ',' |]
        |> Array.map (fun n -> n.Replace("\"", ""))
        |> Array.sort
    names


let totalScore =
    let nameScore (name: string) = name.ToCharArray() |> Array.sumBy (fun l -> lettersNumberMapping.[l])
    names
    |> Array.mapi (fun i n -> int64 ((i + 1) * (nameScore n)))
    |> Array.sum

printfn "Problem 22: %i" totalScore

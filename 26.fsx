(*
A unit fraction contains 1 in the numerator. The decimal representation
of the unit fractions with denominators 2 to 10 are given:

    1/2  =   0.5
    1/3  =   0.(3)
    1/4  =   0.25
    1/5  =   0.2
    1/6  =   0.1(6)
    1/7  =   0.(142857)
    1/8  =   0.125
    1/9  =   0.(1)
    1/10=   0.1

Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle.
It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring
cycle in its decimal fraction part.
*)

let cycleLength d =

    let rec innerCycleLength nr divided =
        let nr' = (nr * 10)
        let remainder = nr' % d
        if nr <> 0 && not (divided |> List.contains nr')
        then innerCycleLength remainder (nr' :: divided)
        else divided.Length
    innerCycleLength 1 []

// tests
if cycleLength 7 <> 6 then failwith "Cycle length of 1/7 <> 6"
if cycleLength 9 <> 1 then failwith "Cycle length of 1/9 <> 1"

let maxCycle =
    [ 2 .. 999 ]
    |> List.map (fun i -> (i, (cycleLength i)))
    |> List.maxBy (fun (i, cl) -> cl)

let a, size = maxCycle

printfn "Result: %i, recurring cycle length %i" (int a) size

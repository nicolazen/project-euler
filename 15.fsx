(*
Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down,
there are exactly 6 routes to the bottom right corner.

How many such routes are there through a 20×20 grid?
*)

// BRUTE FORCE

// let mutable paths = 0

// let rec router (current: int * int) gridSize =
//     let x, y = current
//     let x' = x + 1
//     let y' = y + 1
//     if x' <= gridSize then router (x', y) gridSize
//     if y' <= gridSize then router (x, y') gridSize
//     if (x, y) = (gridSize, gridSize) then paths <- (paths + 1)

// router (0, 0) 20


// printfn "%i" paths

// END BRUTE FORCE

// Start smarter solution using Pascal's triangle

let gridSize = bigint 20
let factorial n = [ (bigint 1) .. n ] |> List.reduce (*)
let result = factorial (gridSize + gridSize) / (factorial gridSize * factorial gridSize)

printfn "%i" (int64 result)

(*
You are given the following information, but you may prefer to do some research
for yourself.

    1 Jan 1900 was a Monday.
    Thirty days has September,
    April, June and November.
    All the rest have thirty-one,
    Saving February alone,
    Which has twenty-eight, rain or shine.
    And on leap years, twenty-nine.

A leap year occurs on any year evenly divisible by 4, but not on a century
unless it is divisible by 400.

How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
*)

open System

let makeDateRange (startDate: DateTime) endDate =
    Seq.initInfinite float
    |> Seq.map startDate.AddDays
    |> Seq.takeWhile (fun dt -> dt <= endDate)

let dateRange = makeDateRange (DateTime(1901, 1, 1)) (DateTime(2000, 12, 31))

// need to find a better way of working with C# enums.
let sundaysFirstOfMonth = dateRange |> Seq.filter (fun d -> d.DayOfWeek = DayOfWeek.Sunday && d.Day = 1)

let result = sundaysFirstOfMonth |> Seq.length

printfn "Problem 19: %i" result

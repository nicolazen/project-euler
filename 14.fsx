(*
The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
*)


let sequenceLength start =
    let rec sequence nr length =
        match nr with
        | 1L -> length
        | _ ->
            match nr % 2L = 0L with
            | true -> sequence (nr / 2L) (length + 1)
            | false -> sequence (3L * nr + 1L) (length + 1)

    let length = sequence start 1
    length

let result max =
    [ 2L .. max ]
    |> List.map (fun nr -> (nr, sequenceLength nr))
    |> List.maxBy (fun (_, y) -> y)

printfn "%i -> %i length" <|| result 1000000L

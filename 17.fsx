(*
If the numbers 1 to 5 are written out in words: one, two, three, four, five,
then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
If all the numbers from 1 to 1000 (one thousand) inclusive were written out in
words,
how many letters would be used?
NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters.
The use of "and" when writing out numbers is in compliance with British usage.
*)

let singles =
    dict
        [ 1, "one"
          2, "two"
          3, "three"
          4, "four"
          5, "five"
          6, "six"
          7, "seven"
          8, "eight"
          9, "nine" ]

let tens =
    dict
        [ 10, "ten"
          2, "twenty"
          3, "thirty"
          4, "forty"
          5, "fifty"
          6, "sixty"
          7, "seventy"
          8, "eighty"
          9, "ninety"
          11, "eleven"
          12, "twelve"
          13, "thirteen"
          14, "fourteen"
          15, "fifteen"
          16, "sixteen"
          17, "seventeen"
          18, "eighteen"
          19, "nineteen" ]

let numberToWord nr =
    let rec convert str nr =
        if nr = 1000 then
            convert (str + "one thousand") (nr % 1000)
        elif nr >= 100 then
            convert (str + singles.Item(nr / 100) + " hundred " + (if (nr % 100 > 0) then "and " else "")) (nr % 100)
        elif nr >= 20 then
            convert (str + tens.Item(nr / 10) + (if (nr % 10 > 0) then "-" else "")) (nr % 10)
        elif nr > 9 then
            convert (str + tens.Item(nr)) (0)
        elif nr > 0 then
            convert (str + singles.Item(nr)) (0)
        else
            str
    convert "" nr

let result =
    [ 1 .. 1000 ]
    |> List.map (fun nr -> (numberToWord nr).Replace(" ", "").Replace("-", ""))
    |> List.reduce (+)
    |> String.length

printfn "%i" result

(*
n! means n × (n − 1) × ... × 3 × 2 × 1

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!
*)

let factorial nr =
    [ 1 .. nr ]
    |> List.map bigint
    |> List.reduce (*)

let problem20 nr =
    (string (factorial nr)).ToCharArray()
    |> Array.map (string >> int)
    |> Array.reduce (+)

printfn "Problem 20: %i" (problem20 100)

(*
Euler discovered the remarkable quadratic formula:

n2+n+41

It turns out that the formula will produce 40 primes for the consecutive integer
values 0≤n≤39 . However, when n=40,402+40+41=40(40+1)+41 is divisible by 41, and
certainly when n=41,412+41+41 is clearly divisible by 41.

The incredible formula

n2−79n+1601

was discovered, which produces 80 primes for the consecutive values 0≤n≤79 . The
product of the coefficients, −79 and 1601, is −126479.

Considering quadratics of the form:

n2+an+b

where |a|<1000 and |b|≤1000, where |n| is the modulus/absolute value of n e.g.
|11|=11 and |−4|=4

Find the product of the coefficients, a and b, for the quadratic expression that
produces the maximum number of primes for consecutive values of n, starting with
n=0.
*)

// source: http://fssnip.net/7E/title/Prime-testing
let isPrime n =
    let n = abs n
    match n with
    | _ when n > 3 && (n % 2 = 0 || n % 3 = 0) -> false
    | _ ->
        let maxDiv = int (System.Math.Sqrt(float n)) + 1

        let rec f d i =
            if d > maxDiv then true
            else if n % d = 0 then false
            else f (d + i) (6 - i)
        f 5 2

let pairs =
    [ for a in -999 .. 999 do
        for b in -1000 .. 1000 do
            (a, b) ]

let quadratic a b n = ((n * n) + (a * n) + b)

let nrConsecutivePrimes pair =
    let (a, b) = pair

    let length =
        Seq.initInfinite id
        |> Seq.takeWhile (fun n -> (quadratic a b n) |> isPrime)
        |> Seq.length
    (a, b, length)

// tests
if (nrConsecutivePrimes (1, 41)) <> (1, 41, 40) then failwith "Euler series length <> 40"
if (nrConsecutivePrimes (-79, 1601)) <> (-79, 1601, 80) then failwith "Euler series length <> 80"

let (a, b, length) =
    pairs
    |> List.map nrConsecutivePrimes
    |> List.maxBy (fun (_, _, l) -> l)

printfn "RESULT => (a: %i, b: %i, length: %i) => product: %i" a b length (a * b)

(*
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
Find the sum of all the primes below two million.
*)

let result =
    let isPrime nr =
        let rec factor smallerNr =
            if nr % smallerNr = 0L then
                match smallerNr with
                | 1L -> true
                | _ -> false
            else
                factor (smallerNr - 1L)

        if nr = 1L then
            true
        elif (nr % 2L = 0L || nr % 3L = 0L || nr % 5L = 0L)
             && nr > 5L then
            false
        else
            let nr' = float nr |> sqrt |> int64
            factor (nr')

    Seq.initInfinite (fun n -> int64 (n + 2))
    |> Seq.filter isPrime
    |> Seq.takeWhile (fun n -> n < 2000000L)
    |> Seq.sum

printfn "%i" result

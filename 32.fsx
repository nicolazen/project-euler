(*
We shall say that an n-digit number is pandigital if it makes use of all the
digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through
5 pandigital.

The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing
multiplicand, multiplier, and product is 1 through 9 pandigital.

Find the sum of all products whose multiplicand/multiplier/product identity can
be written as a 1 through 9 pandigital.
HINT: Some products can be obtained in more than one way so be sure to only
include it once in your sum.
*)

// from https://stackoverflow.com/questions/1222185/most-elegant-combinations-of-elements-in-f
let rec combinations n l =
    match n, l with
    | 0, _ -> [ [] ]
    | _, [] -> []
    | k, (x :: xs) -> List.map ((@) [ x ]) (combinations (k - 1) xs) @ combinations k xs

// still a bit fuzzy on this one and the next. Taken from another SO thread for another problem.
let rec insertions element others =
    match others with
    | [] -> [ [ element ] ]
    | head :: tail -> (element :: others) :: (List.map (fun x -> head :: x) (insertions element tail))

let rec permutations elements =
    match elements with
    | [] -> seq [ [] ]
    | traveler :: others -> Seq.collect (insertions traveler) (permutations others)

// get all permutation of all combinations of length 5 of the 9 possible digits
let fiveDigits =
    [ for a in combinations 5 [ 1; 2; 3; 4; 5; 6; 7; 8; 9 ] do
        permutations a ]

// helper function to go from list of integers to integers without having to
// cast to string, concatenate, and cast to int again. Supposedly faster.
let concatenateDigits (l: int list) =
    let rec concatenator l nr =
        match l with
        | [] -> nr
        | x :: xs -> concatenator xs (nr + x * (pown 10 (l.Length - 1)))
    concatenator l 0

// to make a total of 9 digits we have two options:
//      x * xxxx = xxxx
//      xx * xxx = xxxx
let multipicationTriplets =
    [ for sequence in fiveDigits do
        for digits in sequence do
            for i in 0 .. 1 do
                let multiplicand = concatenateDigits digits.[0..i]
                let multiplier = concatenateDigits digits.[i + 1..5]
                let product = multiplicand * multiplier
                [ multiplicand; multiplier; product ] ]

// Merge multiplicand, multiplier and product in a 9 digits string. Using string
// here because of the next function
let mergeNrs l =
    l
    |> List.map string
    |> List.fold (+) ""

// Split string, sort and compare with the unique 9 digits
let isPandigital (str: string) =
    str.ToCharArray()
    |> Array.sort = [| '1'; '2'; '3'; '4'; '5'; '6'; '7'; '8'; '9' |]

// Filter all triplets to only leave the pandigital ones
let pandigitalIdentities = multipicationTriplets |> List.filter (mergeNrs >> isPandigital)

// remove duplicates and sum. Voila!
let result =
    pandigitalIdentities
    |> List.map (fun x -> x.[2])
    |> set
    |> Set.fold (+) 0

printfn "Result: %i" result

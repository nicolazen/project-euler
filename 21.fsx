(*
Let d(n) be defined as the sum of proper divisors of n (numbers less than n
which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and
each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55
and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and
142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
*)

let d nr =
    [ 1 .. (nr / 2 + 1) ]
    |> List.filter (fun x -> nr % x = 0)
    |> List.sum

assert (d 284 = 220)
assert (d 220 = 284)

let rec findAmicables nr max (amicables: int list) (partials: Map<int, int>) =
    if nr = max then
        amicables
    else
        let sumDivisors = d nr
        // check if we got a d = nr, and that is coming from nr = d
        if partials.TryGetValue(nr) = (true, sumDivisors)
        // we do, so we found 2 amicable numbers and we can add them to the
        // results list
        then findAmicables (nr + 1) max (nr :: sumDivisors :: amicables) partials
        // we don't so we add the result to the partials for checking later
        // agains other results. Partials is KV storage of results.
        else findAmicables (nr + 1) max amicables (Map.add sumDivisors nr partials)

let result =
    let amicables = findAmicables 1 9999 [] Map.empty
    amicables |> List.sum

printfn "Problem 21: %i" result

(*
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in
attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is
correct, is obtained by cancelling the 9s.

We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, less than
one in value, and containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, find
the value of the denominator.
*)

// generate list with all fractions with two digits at both numerator and
// denominator that are < 1
let pairs =
    [ for n in 10 .. 98 do
        for d in n + 1 .. 99 -> (n, d) ]

// Get the number that is in common between numerator and denominator and that
// makes the fraction "tricky" to simplify
let numberInCommon pair =
    let (n, d) = pair
    // Using set arithmetics here. Get the digits, cast to set and then get the
    // intersection between them
    let nDigits = (string n).ToCharArray() |> set
    let dDigits = (string d).ToCharArray() |> set
    (nDigits, dDigits)
    ||> Set.intersect
    |> Set.toList
    |> List.map string

let trickyFractions =
    let hasNumberInCommon pair = numberInCommon pair |> (<>) []

    pairs
    |> List.filter hasNumberInCommon
    // Filter out multiple of tens as they are "trivial examples"
    |> List.filter (fun (a, b) -> a % 10 <> 0 && b % 10 <> 0)

let result =
    // Identify the four fractions that are requested by the problem
    let isSpecialTricky pair =
        let (n, d) = pair
        let common = numberInCommon pair
        // Simplify as the inexperienced student would, by removing the common
        // digit
        let n' = (string n).Replace(common.[0], "")
        let d' = (string d).Replace(common.[0], "")
        // Exclude invalid divisions, like inf and DIV/0, or numbers with the
        // repeated common digits which end up being an empty string
        if n' <> "" && d' <> "" && n' <> "0" && d' <> "0"
        then float n / float d = float n' / float d'
        else false

    let realTrickyOnes = trickyFractions |> List.filter isSpecialTricky

    // mulitply fractions, (numerator * numerator) / (denominator * denominator)
    let reducer p1 p2 =
        let (n1, d1) = p1
        let (n2, d2) = p2
        (n1 * n2, d1 * d2)

    let (n, d) = realTrickyOnes |> List.reduce reducer

    // the problem asks for the lowest common terms. Just by printing the result
    // we can see that the fraction can be simplified as 1/100, so simply doing
    // that in a non generic way. Too lazy today.
    d / n

printfn "Result: %i" result

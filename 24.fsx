(*
A permutation is an ordered arrangement of objects. For example, 3124 is one
possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are
listed numerically or alphabetically, we call it lexicographic order. The
lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5,
6, 7, 8 and 9?
*)

let rec insertions element others =
    match others with
    | [] -> [ [ element ] ]
    | head :: tail -> (element :: others) :: (List.map (fun x -> head :: x) (insertions element tail))

let rec permutations elements =
    match elements with
    | [] -> seq [ [] ]
    | traveler :: others -> Seq.collect (insertions traveler) (permutations others)

let result =
    let mergeDigits p =
        p
        |> List.map string
        |> List.reduce (+)
        |> int64
    permutations [ 0; 1; 2; 3; 4; 5; 6; 7; 8; 9 ]
    |> Seq.map mergeDigits
    |> Seq.toList
    |> List.sort
    |> List.item 999999

printfn "RESULT => %i" result

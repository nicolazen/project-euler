(*
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
What is the 10 001st prime number?
*)

let result =
    let isPrime nr =
        let rec factor smallerNr =
            if nr % smallerNr = 0L then
                match smallerNr with
                | 1L -> true
                | _ -> false
            else
                factor (smallerNr - 1L)

        if nr = 1L then
            true
        elif (nr % 2L = 0L || nr % 3L = 0L || nr % 5L = 0L)
             && nr > 5L then
            false
        else
            factor (nr - 1L)

    Seq.initInfinite (fun n -> int64 (n + 2))
    |> Seq.filter isPrime
    |> Seq.take 10001
    |> Seq.last

printfn "%i" result

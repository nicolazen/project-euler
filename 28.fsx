(*


Starting with the number 1 and moving to the right in a clockwise direction a 5
by 5 spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101.

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed
in the same way?

*)

let square sideLength =
    // we can look at the square as a succession of four numbers each time spaced
    // by a distance that is greater by 2 wrt the previous distances. No need to
    // actually create the whole square and extract the diagonals
    let rec squareGenerator start step (innerLayers: int list list) =
        if ((innerLayers.Length) * 2 + 1) > sideLength then
            innerLayers
        else
            let final = (start + step * 4)
            let corners = [ (start + step) .. step .. final ]
            squareGenerator (final) (step + 2) (corners :: innerLayers)


    squareGenerator 1 2 [ [ 1 ] ]

let problem28 sideLength =
    square sideLength
    |> List.concat
    |> List.sum

// test
if (problem28 5) <> 101 then failwith "Something wrong"

printfn "Result => %i" (problem28 1001)

let products =
    let nrs = [ 100 .. 999 ]
    [ for a in nrs do
        for b in nrs do
            yield (a * b) ]

let isPalindrome nr =
    let digits = (string nr).ToCharArray()
    digits = Array.rev digits

let result =
    products |> Seq.filter isPalindrome |> Seq.max

printfn "%i" result